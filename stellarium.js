const scene = new THREE.Scene();

const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

const anillo = new THREE.RingGeometry(75, 73, 10000);
const material2 = new THREE.MeshBasicMaterial({ color: 0xFFFFFF, side: THREE.DoubleSide });
const ring = new THREE.Mesh(anillo, material2);
scene.add(ring);


const circulo = new THREE.CircleGeometry();
const material1 = new THREE.MeshBasicMaterial({ color: 0xFFFF00 });
const circle = new THREE.Mesh(circulo, material1);
scene.add(circle);



camera.position.z = 100;


const animate = function () {
	requestAnimationFrame(animate);

	
	circle.position.x = 10;
	circle.position.y = 20;


	ring.position.x = 0;
	ring.position.y = 0;


	renderer.render(scene, camera);

};
animate();


let ys

n = (30 * eleccionmes) - (elecciondia - 30)

h = (eleccionhora - 12) + eleccionmin

z = (2(Math.PI) / 365)((n + 1) + ((h - 12) / 24))

decl = ((0.006918) - ((0.399912)(Math.cos(x))) + ((0.070257)(Math.sin(x))) - ((0.006758)(Math.cos(2 * x))) + ((0.000907)(Math.sin(2 * x))) - ((0.002697)(Math.cos(3 * x))) + ((0.001480)(Math.sin(3 * x))))

w = Math.acos((-Math.tan(latitud))(Math.tan(decl)))


Altsol = Math.asin(((Math.cos(latitud))(Math.cos(w))(Math.cos(decl))) + ((Math.sin(latitud))(Math.sin(decl))))

yc = Math.acos((((Math.cos(latitud))(Math.sin(decl))) - ((Math.cos(w))(Math.sin(latitud))(Math.cos(decl)))) / (Math.cos(Altsol)))


if (w < 0) {
	ys = yc
}
else
	if (w > 0) {
		ys = 360 - yc
	}


